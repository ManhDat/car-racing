﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinScript : MonoBehaviour {
	public float speed;
	Rigidbody2D m_rb;
	private void Awake()
	{
		m_rb = GetComponent<Rigidbody2D>();
		
	}
	private void Update() 
	{
		m_rb.velocity = Vector2.down * speed;
		
	}
	// void OnTriggerEnter2D (Collider2D col)
	// {
	// 	if(col.CompareTag(Const.PLAYER_TAG))
	// 	GameManager.Ins.m_coin = +1;
	// 	Destroy (gameObject);
	// }
}
