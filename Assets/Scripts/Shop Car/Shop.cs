﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Shop : MonoBehaviour
{
    [Header("List of items sold")]
    [SerializeField] private ShopItem[] shopItems;
    [Header("References")]
    [SerializeField] private Transform shopContainer;
    [SerializeField] private GameObject shopitemPrefabs;

    private void Start() 
    {
        PopulateShop();
    }
    private void PopulateShop()
    {
        for (int i = 0; i < shopItems.Length; i++)
        {
            ShopItem si = shopItems[i];
            GameObject itemGameobject = Instantiate(shopitemPrefabs,shopContainer);

            // ShopItem (Image, Button, Name )
            // - Icon
            // - Sprite
            // - Name 
            // ChangeBackground Color
            itemGameobject.GetComponent<Image>().color = si.backgroundColor;
            // Change the item's sprite
            itemGameobject.transform.GetChild(0).GetComponent<Image>().sprite = si.sprite;
            // Change item's name 
            itemGameobject.transform.GetChild(1).GetComponent<Text>().text = si.itemName;
            //Change Price
            itemGameobject.transform.GetChild(2).GetComponent<Text>().text = si.price.ToString();
            //On button Click
            itemGameobject.transform.GetChild(3).GetComponent<Button>().onClick.AddListener(() => OnButtonClick(si));
            
        }
    }
    private void OnButtonClick(ShopItem item)
    {
    Debug.Log("hello");
    }
}
