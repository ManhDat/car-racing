﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

 [CreateAssetMenu(menuName = "Shop/Shop Item")]
public class ShopItem : ScriptableObject
{
    public Color backgroundColor;
    public Sprite sprite;
    public string itemName;
    public int price;
    public Button buyBtn;
 

}
