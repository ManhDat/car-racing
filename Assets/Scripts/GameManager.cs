﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    public float minSpawnTime;
    public float maxSpawnTime;
    public GameObject[] obstaclePbs;
    public GameObject player;
    public BGLoop bgLoop;
    public bool isGameOver;
    public bool isGamePlaying;
    private int m_score;
    public int m_coin;

    public int Score { get => m_score; set => m_score = value; }
    public int Coin { get => m_coin; set => m_coin = value; }

    public override void Awake()
    {
        MakeSingleton(false);
    }
    public override void Start()
    {
        base.Start();

        GameGUIManager.Ins.ShowGameGui(false);
        GameGUIManager.Ins.UpdateScoreCouting(m_score);
        GameGUIManager.Ins.UpdateCoinCouting(m_coin);
    }
    public void PlayGame()
    {
        GameGUIManager.Ins.homeGui.SetActive(false);
        StartCoroutine(CoutingDown());
    }
    IEnumerator CoutingDown() // đến thời gian ở text đếm số để bắt đầu game
    {
        float time = 3f;
        GameGUIManager.Ins.UpdateTimeCouting(time);
        while(time > 0)
        {
            yield return new WaitForSeconds(1f);
            time--;
            GameGUIManager.Ins.UpdateTimeCouting(time);
            AudioController.Ins.PlaySound(AudioController.Ins.timeBeep);
        }
        isGamePlaying = true;
        
        if (player)
            player.SetActive(true);
        
        if(bgLoop)
            bgLoop.isStart = true;

        StartCoroutine(SpawnObstacle());
        GameGUIManager.Ins.ShowGameGui(true);
        AudioController.Ins.PlayBackgroundMusic();
    }
    IEnumerator SpawnObstacle() // sinh ra obstacle (car enemy)
    {
        while (!isGameOver)
        {
            float spawntime = Random.Range(minSpawnTime, maxSpawnTime);

            yield return new WaitForSeconds(spawntime);

            if(obstaclePbs != null && obstaclePbs.Length > 0)
            {
                int obIdx = Random.Range(0, obstaclePbs.Length);
                GameObject obstacle = obstaclePbs[obIdx];
                
                if (obstacle)
                {
                    Vector3 spawnPos = new Vector3(Random.Range(-1.4f, 1.4f), 8f, 0f);

                    Instantiate(obstacle, spawnPos, Quaternion.identity);
                } 
            } 
        }
    }

    public void GameOver()
    {
        isGameOver = true;
        isGamePlaying = false;
        Prefs.bestScore = m_score;
        Prefs.Coin = m_coin;
        GameGUIManager.Ins.gameoverDialog.Show(true);
        AudioController.Ins.StopPlayMusic();
        AudioController.Ins.PlaySound(AudioController.Ins.explosion);
    }

    // public override bool Equals(object obj)
    // {
    //     return obj is GameManager manager &&
    //            base.Equals(obj) &&
    //            Coin == manager.Coin;
    // }
}
