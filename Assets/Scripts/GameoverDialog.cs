﻿                using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameoverDialog : Dialog
{
    public Text scoreText;
    public Text bestScoreText;
    public Text coin;

    private void Start() 
    {
        if(scoreText)
            scoreText.text = GameManager.Ins.Score.ToString();
        
        if (bestScoreText)
            bestScoreText.text = Prefs.bestScore.ToString();
        if (coin)
            coin.text = Prefs.Coin.ToString();
    }
    public void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name); 
    }
    public void ExitGame ()
    {
        Application.Quit();
    }
}
