﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Prefs
{
    public static int bestScore
    {
        set
        {
            if (PlayerPrefs.GetInt(Const.BEST_SCORCE_KEY, 0) < value)
            {
                PlayerPrefs.SetInt(Const.BEST_SCORCE_KEY,value);
            }
        }
        get => PlayerPrefs.GetInt(Const.BEST_SCORCE_KEY, 0);
        
    }
    public static int Coin
    {
        set
        {
            if (PlayerPrefs.GetInt(Const.COIN_KEY,0) !=  value)
            {
                PlayerPrefs.SetInt(Const.COIN_KEY,value);
            }
        }
        get => PlayerPrefs.GetInt(Const.COIN_KEY,0);
    }
}
