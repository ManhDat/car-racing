﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameGUIManager : Singleton<GameGUIManager>
{
    public GameObject homeGui;
    public GameObject gameGui;
    public Text timeCoutingText;
    public Text scoreCoutingText;
    public Text coinCoutingText;
    public Dialog gameoverDialog;
    public Dialog pauseDialog;
    public override void Awake() 
    {
        MakeSingleton(false);        
    }
    public void ShowGameGui(bool isShow)
    {
        if (gameGui)
            gameGui.SetActive(isShow);
        
        if (homeGui)
            homeGui.SetActive(!isShow);
    }
    public void UpdateTimeCouting(float time)
    {
        if(timeCoutingText)
        {
            timeCoutingText.gameObject.SetActive(true);
            timeCoutingText.text = time.ToString();
            if(time <=0 )
            {
                timeCoutingText.gameObject.SetActive(false);
            }
        }
    }
    public void UpdateScoreCouting(int score)
    {
        if (scoreCoutingText)
            scoreCoutingText.text="Score:" + score.ToString();
    }
    public void UpdateCoinCouting(int coin)
    {
        if (coinCoutingText)
            coinCoutingText.text = " " + coin.ToString();
    }
    public void PauseGame()
    {
        Time.timeScale = 0f;
        if (pauseDialog)
            pauseDialog.Show(true);
    }
}
